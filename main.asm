; *****************************************************
; base file for lab2
; *****************************************************
    .model small
    .stack 100h
    .386

    include consts.inc
    include macros.mac

    .data
    sline db 78 dup (CHSEP), 0
    minis db "��������⢮ ��ࠧ������ ��ᨩ᪮� 䥤��樨", 0
    ulstu db "���ﭮ�᪨� ���㤠��⢥��� �孨�᪨� 㭨������", 0
    dept db "��䥤� ���᫨⥫쭮� �孨��", 0
    mop db "��設��-�ਥ��஢����� �ணࠬ��஢����", 0
    labr db "������ୠ� ࠡ�� �2", 0
    fio db "���誮� �.�.", 0
    emptys db 0
    entr db "������ ��ப� ⥪��: ", 0FFh
    res_str db "���쪮 ������஬�:     ", 0FFh
    buf db 102 dup (0)

    .code
main label near
    mov ax, @data
    mov ds, ax
    PUTL emptys
	PUTL sline
	PUTL emptys
	PUTLSC minis
	PUTL emptys
	PUTLSC ulstu
	PUTL emptys
	PUTLSC dept
	PUTL emptys
	PUTLSC mop
	PUTL emptys
	PUTLSC labr
	PUTL emptys
    PUTLSC fio
	PUTL emptys
    PUTL sline

    INPT entr, buf, 100
    lea bx, buf+2
    PUTL res_str
    xor si, si  ;������ ��砫�
    xor di, di  ;������ ����
    mov ax, -1  ;�㬥��� ���ᨢ�� � 0, �㦭� ��� �������樨 
    push ax     ;� �⥪� ᢥ��� �࠭����
                ;������⢮ �ன������ ᨬ�����
lp:
    cmp byte ptr [bx][di], 0
    je ext
        call split
        SHIFT_LEN   ;�� ᪮�쪮 ᬥ�⨫���?
        call check_palindrome
        jnc not_show
        call out_word
    jmp lp
    not_show:
        call fill_space
        mov si, di
    jmp lp
ext:
    NWLN
    EXIT
    extrn	putss:  near
    extrn	putc:   near
	extrn   slen:   near
    extrn   gets:   near
    extrn   split:  near
    extrn   out_word:   near
    extrn   fill_space: near
    extrn   check_palindrome:   near
end main
