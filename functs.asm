;*********************************************************
; ��楤��� ��� �祡���� �ਬ�� ������୮� ࠡ��� N 1  *
; �� ���.                                                 *
;*********************************************************
.model small
    .386
    include consts.inc
    include macros.mac
    .code
 locals

 ;=====================================================
 ; ����ணࠬ�� �뢮�� �� ��࠭ ��ப�, ����㥬�� SI,
 ; � ����প�� �६��� ����� ᨬ������ � <CX,DX> mcs.
 ; ������⥫ﬨ ��ப� ����� ����� 0 ��� 0FFh.
 ; ���� ��ப� �����稢����� ���⮬ 0,
 ; �� ���������� ���室 � ��砫� ����� ��ப�
 ;=====================================================
putss proc near
@@l:
    mov al,	[si]
    cmp al, 0ffh
    je @@r
    cmp al, 0
    jz @@e
    call putc
    inc si
    jmp	short @@l
    ; ���室 �� ᫥������ ��ப�
@@e:
    mov	al, CHCR
    call putc
    mov	al, CHLF
    call putc
@@r:
    ret
putss endp

 ;==============================================
 ; ����ணࠬ�� �뢮�� al �� �ନ���
 ;==============================================
putc proc near
    push ax
    push dx
    mov	dl, al
    mov	ah, FUPUTC
    int	DOSFU
    pop	dx
    pop ax
    ret
putc endp

 ;==============================================
 ; ����ணࠬ�� ����� ᨬ���� � al � �ନ����
 ;==============================================
getch proc near
    mov ah, FUGETCH
    int DOSFU
    ret
getch endp

 ;=================================================
 ; ����ணࠬ�� ����� ��ப� � ����, ����㥬� dx
 ;   � ����騩 ��������:
 ;    { char size; // ࠧ��� ����
 ;      char len;  // ॠ�쭮 �������
 ;      char str[size]; // ᨬ���� ��ப� }
 ;=================================================
gets proc near
    push ax
 	push si
 	mov si, dx
    mov	ah,	FUGETS
    int	DOSFU
 	; �ய���� ���� 0 � ����� ��ப�
 	xor	ah,	ah
 	mov	al,	[si+1]
 	add	si,	ax
 	mov	byte ptr [si+2], 0
 	pop	si
    pop ax
    ret
gets endp

 ;==============================================
 ; ����ணࠬ�� ������ �᫠ ᨬ����� � ��ப�,
 ; ����㥬�� si. ������⥫� ��ப�: 0 � 0ffh
 ; १���� �����頥��� � ax
 ;==============================================
slen proc near
	xor ax, ax
lslen:
    cmp byte ptr [si], 0
	je rslen
    cmp byte ptr [si], 0ffh
	je rslen
	inc ax
	inc si
	jmp short lslen
rslen:
    ret
slen endp


; ==============================================
; �㭪�� ��童����� ᫮�� �� ��ப�, ����㥬��
; � dx, �� ࠧ����⥫� "�஡��", ��稭�� � ����樨
; 㪠������ � ॣ���� si. �஡��� ���᪠����.
; ������⥫� ��ப� - �㫥��� ����
; �����頥�� ���祭��:
; si - ������ ��砫� ᫮��(��ࢠ� �㪢�)
; di - ������ ���� ᫮��(᫥�. ᨬ��� ���� ����)
; ==============================================
split proc near
    call pass_space
    mov si, di
@@lp:
    cmp byte ptr [bx][di], 0
    je @@rt
    cmp byte ptr [bx][di], ' '
    je @@rt
        inc di
    jmp short @@lp
@@sub:
@@rt:
    ret
split endp

; ==============================================
; �㭪�� �ய�᪠ �஡���� � ��ப�
; bx - ���� ��ப�
; di - ������ � ��ப�
; ��६�頥� di ��ࠢ� �� ��ப�, ���� ᨬ���,
; �ᯮ������� �� �⮬� ����� �� �⠭��
; �⫨�� �� �஡���
; ==============================================
pass_space proc near
@@lp:
    cmp byte ptr [bx][di], ' '
    jne @@rt
        inc di
    jmp @@lp
@@rt:
    ret
pass_space endp

; ==============================================
; �㭪�� �஢�ન, ���� �� ᫮��, ����㥬��
; �����ᠬ� si:di ������஬��. � ����ᨬ��� ��
; १���� �஢�ન ��ᠭ��������� ��� ���뢠��
; 䫠� �F
; ��ࠬ����:
; bx - ���� ��ப�
; �����頥�� ���祭��:
; si - ���� ��砫� ᫮��(��ࢠ� �㪢�)
; di - ���� ���� ᫮��(�� ��᫥���� �㪢��)
; CF - १����
; ==============================================
check_palindrome proc near
    push ax
    push bx
    push dx
    push si
    push di

    dec di  ;⥯��� di �� ��᫥���� �㪢�
    mov ax, di  ;����頥� � ax ������ ���� ᫮��
    sub ax, si  ;���⠥� ������ ��砫�, ����砥� �����
    mov dl, 2   ;����� �������
    div dl
    inc al      ;�㦭� ���㣫���� � ������� ��஭�
lp:
    cmp al, 0   ;���� �� ��諨 �� �।��� ᫮��
    je true
        mov dl, [bx][di]    ;����㦠�� � dl �㪢� � ���� ᫮��
        cmp dl, [bx][si]    ;�ࠢ������ � �㪢�� �� ��砫� ᫮��
        jne fls             ;�᫨ �� ᮢ����, ����� �� ������஬
        inc si  ;���� ������ 㢥��稢���
        dec di  ;�ࠢ� 㬥��蠥�
        dec al  ;�⠫� ����� � �।��� �� 1 ᨬ���
    jmp lp
fls:
    clc     ;��� 䫠�� CF
    jmp rt
true:
    stc     ;��⠭���� 䫠�� CF
rt:
    pop di
    pop si
    pop dx
    pop bx
    pop ax
    ret
check_palindrome endp

; ==============================================
; �㭪�� �뢮�� ᫮��, ����㥬��� 2 ॣ���ࠬ�
; ��ࠬ����:
; bx - ���� ��ப�
; si - ���� ��砫� ᫮��(��ࢠ� �㪢�)
; di - ���� ���� ᫮��(�� ��᫥���� �㪢��)
; ==============================================
out_word proc near
    push ax
@@lp:
    cmp si, di
    jg @@rt
        mov al, [bx][si]
        call putc
        inc si
    jmp @@lp
@@rt:
    pop ax
    ret
out_word endp

; ==============================================
; �㭪�� ���������� ��⮪� �஡�����
; ��ࠬ����:
; ax - ������⢮ �஡����
; ==============================================
fill_space proc near
    push ax
    push cx
    xor cx, cx
@@lp:
    cmp cx, ax
    je @@rt
        push ax
        mov al, ' '
        call putc
        pop ax
        inc cx
    jmp @@lp
@@rt:
    pop cx
    pop ax
    ret
fill_space endp

public putss, putc, getch, gets,
public slen, split, out_word, check_palindrome
public fill_space
end
